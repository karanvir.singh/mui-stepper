import React from "react";
import { useForm } from "react-hook-form";

const Form2 = (props) => {
  const nameRegex = /^[A-Za-z]*$/;
  const emailRegex = /^[\w-]+@([\w-]+\.)+[\w-]{2,4}$/;
  const numberRegex = /^(\+\d{1,3}[- ]?)?\d{10}$/;

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  return (
    <form onSubmit={handleSubmit(props.next)}>
      <label>Manager: </label>
      <input
        type="text"
        name="manager"
        placeholder="Enter manager name"
        {...register("manager", {
          required: true,
          minLength: 5,
          maxLength: 30,
          pattern: nameRegex,
        })}
      />
      <br />
      {errors.manager && errors.manager.type === "required" && (
        <span style={{ color: "red" }} role="alert">
          Manager Name is required
        </span>
      )}
      <br />
      <label>Work Email: </label>
      <input
        type="email"
        name="workEmail"
        placeholder="Enter your workemail"
        {...register("workEmail", {
          required: true,
          pattern: emailRegex,
        })}
      />
      <br />
      {errors.workEmail && errors.workEmail.type === "required" && (
        <span style={{ color: "red" }} role="alert">
          Work Email is required
        </span>
      )}
      <br />
      <label>Personal Email: </label>
      <input
        type="email"
        name="personalEmail"
        placeholder="Enter personal email"
        {...register("personalEmail", {
          required: true,
          pattern: emailRegex,
        })}
      />
      <br />
      {errors.personalEmail && errors.personalEmail.type === "required" && (
        <span style={{ color: "red" }} role="alert">
          Personal Email is required
        </span>
      )}
      <br />
      <label>Mobile Num: </label>
      <input
        type="text"
        name="mobile"
        placeholder="Enter your mobile No"
        {...register("mobile", {
          required: true,
          pattern: numberRegex,
        })}
      />
      <br />
      {errors.mobile && errors.mobile.type === "required" && (
        <span style={{ color: "red" }} role="alert">
          mobile no is required
        </span>
      )}
      <br />
      <label>ResidencePhone Num: </label>
      <input
        type="text"
        name="residencePhone"
        placeholder="Enter your mobile No"
        {...register("residencePhone", {
          required: true,
          pattern: numberRegex,
        })}
      />
      <br />
      {errors.residencePhone && errors.residencePhone.type === "required" && (
        <span style={{ color: "red" }} role="alert">
          residencePhone no is required
        </span>
      )}
      <br />
      <label>workPhone Num: </label>
      <input
        type="text"
        name="workPhone"
        placeholder="Enter your mobile No"
        {...register("workPhone", {
          required: true,
          pattern: numberRegex,
        })}
      />
      <br />
      {errors.workPhone && errors.workPhone.type === "required" && (
        <span style={{ color: "red" }} role="alert">
          workPhone no is required
        </span>
      )}
      <br />
      <label>Skype Id: </label>
      <input
        type="text"
        name="Skype"
        placeholder="Enter your Skype id"
        {...register("Skype", {
          required: true,
          pattern: emailRegex,
        })}
      />
      <br />
      {errors.Skype && errors.Skype.type === "required" && (
        <span style={{ color: "red" }} role="alert">
          Skype ID is required
        </span>
      )}
      <br />

      <input type="submit" value="Next" />
    </form>
  );
};
export default Form2;
