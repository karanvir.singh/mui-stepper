import React from "react";
import { useForm } from "react-hook-form";

const Form3 = (props) => {
  const nameRegex = /^[A-Za-z]*$/;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    console.log(data);
    props.next();
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label>degree: </label>
        <input
          type="text"
          name="degree"
          placeholder="Enter your degree"
          {...register("degree", {
            required: true,
            pattern: nameRegex,
          })}
        />
        {errors.degree && errors.degree.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            degree is required
          </span>
        )}
        <br />
        <label>Branch: </label>
        <input
          type="text"
          name="branch"
          placeholder="Enter your branch"
          {...register("branch", {
            required: true,
            pattern: nameRegex,
          })}
        />
        {errors.branch && errors.branch.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            branch is required
          </span>
        )}
        <br />
        <br />
        <label>start Date: </label>
        <input
          type="date"
          name="start"
          placeholder="Enter your degree start date"
          {...register("start", {
            required: true,
          })}
        />
        {errors.start && errors.start.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            start date is required
          </span>
        )}
        <br />
        <br />
        <label>end Date: </label>
        <input
          type="date"
          name="end"
          placeholder="Enter your degree end date"
          {...register("end", {
            required: true,
          })}
        />
        {errors.end && errors.end.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            end date is required
          </span>
        )}
        <br />
        <br />
        <label>percentage: </label>
        <input
          type="number"
          name="percentage"
          placeholder="Enter your percentage"
          {...register("percentage", {
            valueAsNumber: true,
            validate: (value) => value > 0,
          })}
        />
        <br />
        <label>school: </label>
        <input
          type="text"
          name="school"
          placeholder="Enter your school name"
          {...register("school", {
            required: true,
          })}
        />
        <br />
        <label>organization: </label>
        <input
          type="text"
          name="organization"
          placeholder="Enter your organization"
          {...register("organization", {
            required: true,
          })}
        />
        <br /> <label>designation: </label>
        <input
          type="text"
          name="designation"
          placeholder="Enter your designation"
          {...register("designation", {
            required: true,
          })}
        />
        <br />
        <label>org_start: </label>
        <input
          type="text"
          name="org_start"
          placeholder="Enter your org_start"
          {...register("org_start", {
            required: true,
          })}
        />
        <br />
        <label>org_end: </label>
        <input
          type="text"
          name="org_end"
          placeholder="Enter your org_end"
          {...register("org_end", {
            required: true,
          })}
        />
        <br />
        <label>location: </label>
        <input
          type="text"
          name="location"
          placeholder="Enter your location"
          {...register("location", {
            required: true,
            pattern: nameRegex,
          })}
        />
        <br />
        <label>password: </label>
        <input
          type="password"
          name="password"
          placeholder="Enter your location"
          {...register("password", {
            required: true,
            pattern: nameRegex,
          })}
        />
        <br />
        <input type="submit" value="Next" />
      </form>
    </>
  );
};

export default Form3;
