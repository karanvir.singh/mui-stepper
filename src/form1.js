import React from "react";
import { useForm } from "react-hook-form";

const Form1 = (props) => {
  const nameRegex = /^[A-Za-z]*$/;
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    console.log(data);
    props.next();
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label>First Name: </label>
        <input
          id="name"
          placeholder="Enter your first name"
          {...register("firstName", {
            required: true,

            maxLength: 30,
            pattern: nameRegex,
          })}
        />
        <br />
        {errors.firstName && errors.firstName.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            First Name is required
          </span>
        )}
        {errors.firstName && errors.firstName.type === "maxLength" && (
          <span role="alert">Max length exceeded</span>
        )}
        <br />
        <label>Middle Name: </label>
        <input
          type="text"
          name="middleName"
          placeholder="Enter your middlename"
          {...register("middleName", {
            required: true,
            maxLength: 15,
            pattern: nameRegex,
          })}
        />
        <br />
        {errors.middleName && errors.middleName.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            Middle Name is required
          </span>
        )}
        {errors.middleName && errors.middleName.type === "maxLength" && (
          <span role="alert">Max length exceeded</span>
        )}
        <br />
        <label>Last Name: </label>
        <input
          type="text"
          name="lastName"
          placeholder="Enter your Last name"
          {...register("lastName", {
            required: true,
            maxLength: 15,
            pattern: nameRegex,
          })}
        />
        <br />
        {errors.lastName && errors.lastName.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            Last Name is required
          </span>
        )}
        {errors.lastName && errors.lastName.type === "maxLength" && (
          <span role="alert">Max length exceeded</span>
        )}
        {errors.firstName && errors.firstName.type === "pattern" && (
          <span role="alert">Enter valid nam</span>
        )}
        <br />
        Gender :
        <input
          {...(register("gender"), { required: true })}
          type="radio"
          id="Male"
          name="gender"
          value="Male"
        />
         Male
        <input
          type="radio"
          id="Female"
          name="gender"
          value="Female"
          {...(register("gender"), { required: true })}
        />
         Female
        <br></br>
        <br />
        <label>DOB: </label>
        <input
          type="date"
          name="dob"
          {...register("dob", { required: true })}
        />
        <br />
        {errors.dob && errors.dob.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            dob is required
          </span>
        )}
        <br />
        Martial Status :
        <input
          type="radio"
          id="Yes"
          name="maritalStatus"
          value="Yes"
          {...(register("maritalStatus"), { required: true })}
        />
        Yes
        <input
          type="radio"
          id="No"
          name="maritalStatus"
          value="No"
          {...(register("maritalStatus"), { required: true })}
        />
        No
        <br></br> <br />
        <label>Blood Group: </label>
        <select name="bloodGroup" id="bloodGroup">
          <option value="a+">a+</option>
          <option value="a-">a-</option>
          <option value="b+">b+</option>
          <option value="b-">b-</option>
        </select>
        <br />
        {errors.bloodGroup && errors.dob.type === "required" && (
          <span style={{ color: "red" }} role="alert">
            dob is required
          </span>
        )}
        <br />
        <input type="submit" value="Next" />
      </form>
    </>
  );
};
export default Form1;
